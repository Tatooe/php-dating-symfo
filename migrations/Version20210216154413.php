<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210216154413 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE personne (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, departement INT NOT NULL, mail VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_FCEC9EF5126AC48 (mail), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pratique (id INT AUTO_INCREMENT NOT NULL, pers_get_sport_and_niv_id INT NOT NULL, sport_get_pers_and_niv_id INT NOT NULL, niveau VARCHAR(45) NOT NULL, INDEX IDX_1F2B781F34D2E7F (pers_get_sport_and_niv_id), INDEX IDX_1F2B7814A3F241C (sport_get_pers_and_niv_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sport (id INT AUTO_INCREMENT NOT NULL, sport VARCHAR(100) NOT NULL, UNIQUE INDEX UNIQ_1A85EFD21A85EFD2 (sport), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pratique ADD CONSTRAINT FK_1F2B781F34D2E7F FOREIGN KEY (pers_get_sport_and_niv_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE pratique ADD CONSTRAINT FK_1F2B7814A3F241C FOREIGN KEY (sport_get_pers_and_niv_id) REFERENCES sport (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pratique DROP FOREIGN KEY FK_1F2B781F34D2E7F');
        $this->addSql('ALTER TABLE pratique DROP FOREIGN KEY FK_1F2B7814A3F241C');
        $this->addSql('DROP TABLE personne');
        $this->addSql('DROP TABLE pratique');
        $this->addSql('DROP TABLE sport');
    }
}
