<?php

namespace App\Entity;

use App\Repository\SportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SportRepository::class)
 * 
 */
class Sport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     */
    private $sport;

    /**
     * @ORM\OneToMany(targetEntity=Pratique::class, mappedBy="sportGetPersAndNiv", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $sportGetPersAndNiv;

    public function __construct()
    {
        $this->sportGetPersAndNiv = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSport(): ?string
    {
        return $this->sport;
    }

    public function setSport(string $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    /**
     * @return Collection|Pratique[]
     */
    public function getSportGetPersAndNiv(): Collection
    {
        return $this->sportGetPersAndNiv;
    }

    public function addSportGetPersAndNiv(Pratique $sportGetPersAndNiv): self
    {
        if (!$this->sportGetPersAndNiv->contains($sportGetPersAndNiv)) {
            $this->sportGetPersAndNiv[] = $sportGetPersAndNiv;
            $sportGetPersAndNiv->setSportGetPersAndNiv($this);
        }

        return $this;
    }

    public function removeSportGetPersAndNiv(Pratique $sportGetPersAndNiv): self
    {
        if ($this->sportGetPersAndNiv->removeElement($sportGetPersAndNiv)) {
            // set the owning side to null (unless already changed)
            if ($sportGetPersAndNiv->getSportGetPersAndNiv() === $this) {
                $sportGetPersAndNiv->setSportGetPersAndNiv(null);
            }
        }

        return $this;
    }
    
    public function __toString() {
        return $this->getSport();
    }
}
