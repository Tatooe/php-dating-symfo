<?php

namespace App\Entity;

use App\Repository\PersonneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonneRepository::class)
 * 
 */
class Personne
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="integer",nullable=false)
     */
    private $departement;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private $mail;

    /**
     * @ORM\OneToMany(targetEntity=Pratique::class, mappedBy="persGetSportAndNiv", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $persGetSportAndNiv;

    public function __construct()
    {
        $this->persGetSportAndNiv = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDepartement(): ?int
    {
        return $this->departement;
    }

    public function setDepartement(int $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * @return Collection|Pratique[]
     */
    public function getPersGetSportAndNiv(): Collection
    {
        return $this->persGetSportAndNiv;
    }

    public function addPersGetSportAndNiv(Pratique $persGetSportAndNiv): self
    {
        if (!$this->persGetSportAndNiv->contains($persGetSportAndNiv)) {
            $this->persGetSportAndNiv[] = $persGetSportAndNiv;
            $persGetSportAndNiv->setPersGetSportAndNiv($this);
        }

        return $this;
    }

    public function removePersGetSportAndNiv(Pratique $persGetSportAndNiv): self
    {
        if ($this->persGetSportAndNiv->removeElement($persGetSportAndNiv)) {
            // set the owning side to null (unless already changed)
            if ($persGetSportAndNiv->getPersGetSportAndNiv() === $this) {
                $persGetSportAndNiv->setPersGetSportAndNiv(null);
            }
        }

        return $this;
    }
    
    public function __toString():string {
        return $this->getMail();
    }
}
