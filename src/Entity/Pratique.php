<?php

namespace App\Entity;

use App\Repository\PratiqueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PratiqueRepository::class)
 */
class Pratique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45, nullable=false)
     */
    private $niveau;

    /**
     * @ORM\ManyToOne(targetEntity=Personne::class, inversedBy="persGetSportAndNiv",cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $persGetSportAndNiv;

    /**
     * @ORM\ManyToOne(targetEntity=Sport::class, inversedBy="sportGetPersAndNiv", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $sportGetPersAndNiv;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getPersGetSportAndNiv(): ?Personne
    {
        return $this->persGetSportAndNiv;
    }

    public function setPersGetSportAndNiv(?Personne $persGetSportAndNiv): self
    {
        $this->persGetSportAndNiv = $persGetSportAndNiv;

        return $this;
    }

    public function getSportGetPersAndNiv(): ?Sport
    {
        return $this->sportGetPersAndNiv;
    }

    public function setSportGetPersAndNiv(?Sport $sportGetPersAndNiv): self
    {
        $this->sportGetPersAndNiv = $sportGetPersAndNiv;

        return $this;
    }
    
    public function __toString():string{
       return strval($this->getPersGetSportAndNiv());
    }
}
