<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Entity\Sport;
use App\Entity\Pratique;
use App\Form\PratiqueType;
use App\Repository\PratiqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class PratiqueController extends AbstractController
{
    /**
     * @Route("/", name="pratique_index", methods={"GET"})
     */
    public function index(PratiqueRepository $pratiqueRepository): Response
    {
        return $this->render('pratique/index.html.twig', [
            'pratiques' => $pratiqueRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="pratique_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pratique = new Pratique();
        $form = $this->createForm(PratiqueType::class, $pratique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $entityManager = $this->getDoctrine()->getManager();
            
            
//            Recherche si mail connu dans la DBB
            $personSearchIntoDBBPerson = $entityManager->getRepository(Personne::class);            
            $mailId = $personSearchIntoDBBPerson->findOneBy(['mail'=>$pratique->getPersGetSportAndNiv()->getMail()]);  
            
 //           Recherche si le sport est connu dans la BDD            
            $sportSearchIntoDBBSport = $entityManager->getRepository(Sport::class);
            $sportId = $sportSearchIntoDBBSport->findOneBy(['sport'=>$pratique->getSportGetPersAndNiv()->getSport()]);
            
            
            
       
//            Si mail connu alors on rajoute l'id de la personne dans la table pratique
            if($mailId){
               $pratique->setPersGetSportAndNiv($mailId);
            }
     
//            Si sport connu dans la DBB alors on prend l'ID et on l'injecte dans la table pratique
            
            if($sportId & $mailId){
                $pratique->setSportGetPersAndNiv($sportId);
            }    
                       
                   
            $entityManager->persist($pratique);
            $entityManager->flush();

          
                        


            return $this->redirectToRoute('pratique_index');
        }

        return $this->render('pratique/new.html.twig', [
            'pratique' => $pratique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pratique_show", methods={"GET"})
     */
    public function show(Pratique $pratique): Response
    {
        return $this->render('pratique/show.html.twig', [
            'pratique' => $pratique,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pratique_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pratique $pratique): Response
    {
        $form = $this->createForm(PratiqueType::class, $pratique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pratique_index');
        }

        return $this->render('pratique/edit.html.twig', [
            'pratique' => $pratique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pratique_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pratique $pratique): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pratique->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pratique);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pratique_index');
    }
}
