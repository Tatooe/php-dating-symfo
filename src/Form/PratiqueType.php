<?php

namespace App\Form;
use App\Entity\Sport;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Form\PersonneType;
use App\Entity\Pratique;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PratiqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder     
            
            ->add('persGetSportAndNiv', PersonneType::class)
            ->add('sportGetPersAndNiv')               
            ->add('niveau', ChoiceType::class, [
                'choices' => [
                    'débutant'=>'débutant',
                    'confirmé'=>'confirmé',
                    'pro'=>'pro',
                    'supporter'=>'supporter'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pratique::class,
        ]);
    }
}
